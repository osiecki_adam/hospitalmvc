﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HospitalMVC.Models;

namespace HospitalMVC.Controllers
{
    public class HospitalDoctorsController : Controller
    {
        private readonly HospitalMVCContext _context;

        public HospitalDoctorsController(HospitalMVCContext context)
        {
            _context = context;
        }

        // GET: HospitalDoctors
        public async Task<IActionResult> Index(string sortOrder)
        {/*
            var hospitalMVCContext = _context.HospitalDoctors.Include(h => h.Doctor).Include(h => h.Hospital);
           */
            ViewBag.DocIDSortParm = String.IsNullOrEmpty(sortOrder) ? "DoctorID_desc" : "";
            ViewBag.HosIDSortParm = sortOrder == "HospitalID" ? "HospitalID_desc" : "HospitalID";
            var hospitalMVCContext = from h in _context.HospitalDoctors.Include(h => h.Doctor).Include(h => h.Hospital)
                                     select h;

            switch (sortOrder)
            {
                case "DoctorID_desc":
                    hospitalMVCContext = hospitalMVCContext.OrderByDescending(h => h.DoctorID);
                    break;
                case "HospitalID":
                    hospitalMVCContext = hospitalMVCContext.OrderBy(h => h.HospitalID);
                    break;
                case "HospitalID_desc":
                    hospitalMVCContext = hospitalMVCContext.OrderByDescending(h => h.HospitalID);
                    break;
                default:
                    hospitalMVCContext = hospitalMVCContext.OrderBy(h => h.DoctorID);
                    break;
            }

            return View(await hospitalMVCContext.ToListAsync());
        }

        // GET: HospitalDoctors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hospitalDoctor = await _context.HospitalDoctors
                .Include(h => h.Doctor)
                .Include(h => h.Hospital)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (hospitalDoctor == null)
            {
                return NotFound();
            }

            return View(hospitalDoctor);
        }

        // GET: HospitalDoctors/Create
        public IActionResult Create()
        {
            ViewData["DoctorID"] = new SelectList(_context.Doctors, "ID", "FullName");
            ViewData["HospitalID"] = new SelectList(_context.Hospitals, "ID", "Name");
            return View();
        }

        // POST: HospitalDoctors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,HospitalID,DoctorID,DateStarted,Department,Position,Supervisor,Status,PartTime,HomeConsult,LastModifiedBy")] HospitalDoctor hospitalDoctor)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hospitalDoctor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DoctorID"] = new SelectList(_context.Doctors, "ID", "FullName", hospitalDoctor.DoctorID);
            ViewData["HospitalID"] = new SelectList(_context.Hospitals, "ID", "Name", hospitalDoctor.HospitalID);
            return View(hospitalDoctor);
        }

        // GET: HospitalDoctors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hospitalDoctor = await _context.HospitalDoctors.SingleOrDefaultAsync(m => m.ID == id);
            if (hospitalDoctor == null)
            {
                return NotFound();
            }
            ViewData["DoctorID"] = new SelectList(_context.Doctors, "ID", "FullName", hospitalDoctor.DoctorID);
            ViewData["HospitalID"] = new SelectList(_context.Hospitals, "ID", "Name", hospitalDoctor.HospitalID);
            return View(hospitalDoctor);
        }

        // POST: HospitalDoctors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,HospitalID,DoctorID,DateStarted,Department,Position,Supervisor,Status,PartTime,HomeConsult,LastModifiedBy")] HospitalDoctor hospitalDoctor)
        {
            if (id != hospitalDoctor.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hospitalDoctor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HospitalDoctorExists(hospitalDoctor.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DoctorID"] = new SelectList(_context.Doctors, "ID", "FullName", hospitalDoctor.DoctorID);
            ViewData["HospitalID"] = new SelectList(_context.Hospitals, "ID", "Name", hospitalDoctor.HospitalID);
            return View(hospitalDoctor);
        }

        // GET: HospitalDoctors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hospitalDoctor = await _context.HospitalDoctors
                .Include(h => h.Doctor)
                .Include(h => h.Hospital)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (hospitalDoctor == null)
            {
                return NotFound();
            }

            return View(hospitalDoctor);
        }

        // POST: HospitalDoctors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hospitalDoctor = await _context.HospitalDoctors.SingleOrDefaultAsync(m => m.ID == id);
            _context.HospitalDoctors.Remove(hospitalDoctor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HospitalDoctorExists(int id)
        {
            return _context.HospitalDoctors.Any(e => e.ID == id);
        }
    }
}
