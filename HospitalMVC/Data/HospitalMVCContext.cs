﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HospitalMVC.Models;

namespace HospitalMVC.Models
{
    public class HospitalMVCContext : DbContext
    {
        public HospitalMVCContext (DbContextOptions<HospitalMVCContext> options)
            : base(options)
        {
        }

        public DbSet<HospitalMVC.Models.Doctor> Doctors { get; set; }

        public DbSet<HospitalMVC.Models.Hospital> Hospitals { get; set; }

        public DbSet<HospitalMVC.Models.HospitalDoctor> HospitalDoctors { get; set; }
    }
}
