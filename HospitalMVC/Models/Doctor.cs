﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalMVC.Models
{
    public class Doctor
    {

        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Surname { get; set; }

        [Required]
        [MaxLength(25)]
        public string Title { get; set; }


        [Display(Name = "License Number")]
        [Required]
        public int LicenseNumber { get; set; }

        [Required]
        [MaxLength(25)]
        public string Position { get; set; }

        [Required]
        [MaxLength(25)]
        public string Department { get; set; }

        [MaxLength(75)]
        public string Supervisor { get; set; }

        [Display(Name = "Work Phone")]
        [Required]
        [MaxLength(11)]
        public string WorkPhone { get; set; }

        [Display(Name = "Private Phone")]
        [MaxLength(11)]
        public string PrivatePhone { get; set; }

        [EmailAddress]
        [Required]
        [MaxLength(75)]
        public string Email { get; set; }

        [Required]
        [MaxLength(50)]
        public string Country { get; set; }

        [Required]
        [MaxLength(25)]
        public string Town { get; set; }

        [Required]
        [MaxLength(75)]
        public string Street { get; set; }

        [Display(Name = "House Number")]
        [Required]
        [MaxLength(4)]
        public string HouseNumber { get; set; }

        [Display(Name = "Street Number")]
        [Required]
        [MaxLength(4)]
        public string StreetNumber { get; set; }

        [Display(Name = "Postal Code")]
        [Required]
        [MaxLength(11)]
        public string PostalCode { get; set; }

        [Display(Name = "Is Available?")]
        [Required]
        public bool IsAvailable { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
        ApplyFormatInEditMode = true), Display(Name = "Available Date")]
        [Required]
        public DateTime AvailableDate { get; set; }

        [Required]
        [MaxLength(25)]
        public string Status { get; set; }

        [Display(Name = "Part Time")]
        [Required]
        public bool PartTime { get; set; }

        [Display(Name = "Home Consult")]
        [Required]
        public bool HomeConsult { get; set; }

        [Display(Name = "Full Name")]
        public string FullName
        {
            get { return Name + " " + Surname + " Nr Lic. " + LicenseNumber; }
        }

        //Foreign Key
        public ICollection<HospitalDoctor> HospitalDoctors { get; set; }

    }

}
