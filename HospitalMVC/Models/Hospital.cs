﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalMVC.Models
{
    public class Hospital
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Town { get; set; }

        [Required]
        public string Street { get; set; }

        [Display(Name = "Street Number")]
        [Required]
        public string StreetNumber { get; set; }

        [Required]
        public string PostalCode { get; set; }

        [Display(Name = "Phone Number 1")]
        [Required]
        public string PhoneNumber1 { get; set; }

        [Display(Name = "Phone Number 2")]
        public string PhoneNumber2 { get; set; }

        [Display(Name = "Fax Number 1")]
        [Required]
        public string FaxNumber1 { get; set; }

        [Display(Name = "Fax Number 2")]
        public string FaxNumber2 { get; set; }

        [Display(Name = "Chief Of Medicine")]
        [Required]
        public string ChiefOfMedicine { get; set; }

        [Display(Name = "Sales Representative")]
        public string SalesRep { get; set; }

        [Display(Name = "Gps Address")]
        public string GpsAddress { get; set; }

        [Display(Name = "Helicopter Access")]
        [Required]
        public bool HelicopterAccess { get; set; }

        [Display(Name = "Teaching Hospital")]
        [Required]
        public bool TeachingHospital { get; set; }

        //Foreign Key
        public ICollection<HospitalDoctor> HospitalDoctors { get; set; }
    }
}
