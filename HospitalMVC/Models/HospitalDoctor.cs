﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace HospitalMVC.Models
{
    public class HospitalDoctor
    {
        public int ID { get; set; }

        [Display(Name = "Hospital ID")]
        [Required]
        public int HospitalID { get; set; }

        [Display(Name = "Doctor ID")]
        [Required]
        public int DoctorID { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
        ApplyFormatInEditMode = true), Display(Name = "Date Started")]
        [Required]
        public DateTime DateStarted { get; set; }

        [Required]
        [MaxLength(25)]
        public string Department { get; set; }

        [Required]
        [MaxLength(25)]
        public string Position { get; set; }

        [MaxLength(75)]
        public string Supervisor { get; set; }

        [Required]
        [MaxLength(25)]
        public string Status { get; set; }

        [Display(Name = "Part Time")]
        [Required]
        public bool PartTime { get; set; }

        [Display(Name = "Home Consult")]
        [Required]
        public bool HomeConsult { get; set; }

        [Display(Name = "Last Modified By")]
        [MaxLength(25)]
        public string LastModifiedBy { get; set; }

        public virtual Hospital Hospital { get; set; }

        public virtual Doctor Doctor { get; set; }

    }
}
