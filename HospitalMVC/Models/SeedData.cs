﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace HospitalMVC.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new HospitalMVCContext(
                serviceProvider.GetRequiredService<DbContextOptions<HospitalMVCContext>>()))
            {
                // Look for any Doctors.
                if (!(context.Doctors.Any()))
                {
                    context.Doctors.AddRange(
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-23"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail1@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "15",
                         IsAvailable = true,
                         LicenseNumber = 11111,
                         Name = "Paweł",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "20 - 213",
                         PrivatePhone = "123123123",
                         Status = "Status1",
                         Street = "Hutnicza",
                         StreetNumber = "45",
                         Supervisor = "Supervisor1",
                         Surname = "Nowak",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "321321321"
                     },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-24"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail222@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "20",
                         IsAvailable = true,
                         LicenseNumber = 11115,
                         Name = "Marek",
                         PartTime = false,
                         Position = "Position2",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444555111",
                         Status = "Status2",
                         Street = "Lubelska",
                         StreetNumber = "12",
                         Supervisor = "Supervisor1",
                         Surname = "Krawczyk",
                         Title = "M.D.",
                         Town = "Radom",
                         WorkPhone = "451121322"
                     },
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-26"),
                         Country = "Germany",
                         Department = "Cardiology",
                         Email = "example2@gmail.com",
                         HomeConsult = false,
                         HouseNumber = "6",
                         IsAvailable = false,
                         LicenseNumber = 12345,
                         Name = "Paweł",
                         PartTime = false,
                         Position = "Position3",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444999211",
                         Status = "Status3",
                         Street = "Biernata",
                         StreetNumber = "87",
                         Supervisor = "Supervisor2",
                         Surname = "Pietraszek",
                         Title = "M.D.",
                         Town = "Kielce",
                         WorkPhone = "991121322"
                     },

                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-09-10"),
                          Country = "Germany",
                          Department = "Haematology",
                          Email = "example@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 56712,
                          Name = "Michał",
                          PartTime = false,
                          Position = "Position1",
                          PostalCode = "20 - 213",
                          PrivatePhone = "444555211",
                          Status = "Status1",
                          Street = "Zana",
                          StreetNumber = "34",
                          Supervisor = "Supervisor1",
                          Surname = "Kowalski",
                          Title = "M.D.",
                          Town = "Warsaw",
                          WorkPhone = "771124322"
                      },

                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-10-11"),
                          Country = "Poland",
                          Department = "Pharmacy",
                          Email = "mail@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 43713,
                          Name = "Kamil",
                          PartTime = false,
                          Position = "Position2",
                          PostalCode = "20 - 111",
                          PrivatePhone = "442321211",
                          Status = "Status2",
                          Street = "Dolna",
                          StreetNumber = "34",
                          Supervisor = "Supervisor3",
                          Surname = "Andrus",
                          Title = "M.D.",
                          Town = "Olsztyn",
                          WorkPhone = "772224444"
                      },
                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-10-11"),
                          Country = "France",
                          Department = "Physiotherapy",
                          Email = "mail333@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 12353,
                          Name = "Alexandre",
                          PartTime = false,
                          Position = "Position3",
                          PostalCode = "20 - 421",
                          PrivatePhone = "442321222",
                          Status = "Status3",
                          Street = "rue de Talleyrand",
                          StreetNumber = "24",
                          Supervisor = "Supervisor1",
                          Surname = "Coombes",
                          Title = "M.D.",
                          Town = "Paris",
                          WorkPhone = "111554444"
                      },
                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-09-14"),
                          Country = "Ukraine",
                          Department = "Physiotherapy",
                          Email = "example1@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 29303,
                          Name = "Dmitri",
                          PartTime = false,
                          Position = "Position1",
                          PostalCode = "11 - 551",
                          PrivatePhone = "446651222",
                          Status = "Status1",
                          Street = "Antonowycza",
                          StreetNumber = "55",
                          Supervisor = "Supervisor3",
                          Surname = "Berbatov",
                          Title = "M.D.",
                          Town = "Lviv",
                          WorkPhone = "111599994"
                      },
                      new Doctor
                        {
                         AvailableDate = DateTime.Parse("2017-08-23"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail1@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "15",
                         IsAvailable = true,
                         LicenseNumber = 76321,
                         Name = "Michał",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "20 - 213",
                         PrivatePhone = "123123123",
                         Status = "Status1",
                         Street = "Hutnicza",
                         StreetNumber = "45",
                         Supervisor = "Supervisor1",
                         Surname = "Wolski",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "321321321"
                     },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-24"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail222@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "20",
                         IsAvailable = true,
                         LicenseNumber = 87231,
                         Name = "Piotr",
                         PartTime = false,
                         Position = "Position2",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444555111",
                         Status = "Status2",
                         Street = "Lubelska",
                         StreetNumber = "12",
                         Supervisor = "Supervisor1",
                         Surname = "Tokarski",
                         Title = "M.D.",
                         Town = "Radom",
                         WorkPhone = "451121322"
                     },
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2016-05-01"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail99@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "84",
                         IsAvailable = true,
                         LicenseNumber = 54321,
                         Name = "Andrzej",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "99 - 876",
                         PrivatePhone = "999222333",
                         Status = "Status2",
                         Street = "Odlewnicza",
                         StreetNumber = "54",
                         Supervisor = "Supervisor1",
                         Surname = "Walczak",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "765567897"
                     },
                     
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-11-09"),
                         Country = "Poland",
                         Department = "Microbiology",
                         Email = "mail555@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "34",
                         IsAvailable = true,
                         LicenseNumber = 45986,
                         Name = "Maksymilan",
                         PartTime = false,
                         Position = "Position2",
                         PostalCode = "23 - 764",
                         PrivatePhone = "888555999",
                         Status = "Status2",
                         Street = "Warszawska",
                         StreetNumber = "42",
                         Supervisor = "Supervisor1",
                         Surname = "Andrzejczyk",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "459991322"
                     },

                     /*xxx*/
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-23"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail1@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "15",
                         IsAvailable = true,
                         LicenseNumber = 21411,
                         Name = "Piotr",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "20 - 213",
                         PrivatePhone = "123123123",
                         Status = "Status1",
                         Street = "Hutnicza",
                         StreetNumber = "45",
                         Supervisor = "Supervisor1",
                         Surname = "Pawluk",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "321321321"
                     },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-24"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail222@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "20",
                         IsAvailable = true,
                         LicenseNumber = 10115,
                         Name = "Adrian",
                         PartTime = false,
                         Position = "Position2",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444555111",
                         Status = "Status2",
                         Street = "Lubelska",
                         StreetNumber = "12",
                         Supervisor = "Supervisor1",
                         Surname = "Kulpa",
                         Title = "M.D.",
                         Town = "Radom",
                         WorkPhone = "451121322"
                     },
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-26"),
                         Country = "Germany",
                         Department = "Cardiology",
                         Email = "example2@gmail.com",
                         HomeConsult = false,
                         HouseNumber = "6",
                         IsAvailable = false,
                         LicenseNumber = 54345,
                         Name = "Wojciech",
                         PartTime = false,
                         Position = "Position3",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444999211",
                         Status = "Status3",
                         Street = "Biernata",
                         StreetNumber = "87",
                         Supervisor = "Supervisor2",
                         Surname = "Janicki",
                         Title = "M.D.",
                         Town = "Kielce",
                         WorkPhone = "991121322"
                     },

                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-09-10"),
                          Country = "Germany",
                          Department = "Haematology",
                          Email = "example@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 55512,
                          Name = "Michael",
                          PartTime = false,
                          Position = "Position1",
                          PostalCode = "20 - 213",
                          PrivatePhone = "444555211",
                          Status = "Status1",
                          Street = "Zana",
                          StreetNumber = "34",
                          Supervisor = "Supervisor1",
                          Surname = "Muller",
                          Title = "M.D.",
                          Town = "Warsaw",
                          WorkPhone = "771124322"
                      },

                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-10-11"),
                          Country = "Poland",
                          Department = "Pharmacy",
                          Email = "mail@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 23713,
                          Name = "Janusz",
                          PartTime = false,
                          Position = "Position2",
                          PostalCode = "20 - 111",
                          PrivatePhone = "442321211",
                          Status = "Status2",
                          Street = "Dolna",
                          StreetNumber = "34",
                          Supervisor = "Supervisor3",
                          Surname = "Brzeziński",
                          Title = "M.D.",
                          Town = "Olsztyn",
                          WorkPhone = "772224444"
                      },
                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-10-11"),
                          Country = "France",
                          Department = "Physiotherapy",
                          Email = "mail333@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 87653,
                          Name = "Michelle",
                          PartTime = false,
                          Position = "Position3",
                          PostalCode = "20 - 421",
                          PrivatePhone = "442321222",
                          Status = "Status3",
                          Street = "rue de Talleyrand",
                          StreetNumber = "24",
                          Supervisor = "Supervisor1",
                          Surname = "Pierre",
                          Title = "M.D.",
                          Town = "Paris",
                          WorkPhone = "111554444"
                      },
                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-09-14"),
                          Country = "Ukraine",
                          Department = "Physiotherapy",
                          Email = "example1@gmail.com",
                          HomeConsult = false,
                          HouseNumber = "1",
                          IsAvailable = false,
                          LicenseNumber = 90901,
                          Name = "Andriej",
                          PartTime = false,
                          Position = "Position1",
                          PostalCode = "11 - 551",
                          PrivatePhone = "446651222",
                          Status = "Status1",
                          Street = "Antonowycza",
                          StreetNumber = "55",
                          Supervisor = "Supervisor3",
                          Surname = "Shevchenko",
                          Title = "M.D.",
                          Town = "Lviv",
                          WorkPhone = "111599994"
                      },
                      new Doctor
                      {
                          AvailableDate = DateTime.Parse("2017-08-23"),
                          Country = "Poland",
                          Department = "Anaesthetics",
                          Email = "mail1@gmail.com",
                          HomeConsult = true,
                          HouseNumber = "15",
                          IsAvailable = true,
                          LicenseNumber = 99321,
                          Name = "Adam",
                          PartTime = false,
                          Position = "Position1",
                          PostalCode = "20 - 213",
                          PrivatePhone = "123123123",
                          Status = "Status1",
                          Street = "Hutnicza",
                          StreetNumber = "45",
                          Supervisor = "Supervisor1",
                          Surname = "Dobroolski",
                          Title = "M.D.",
                          Town = "Lublin",
                          WorkPhone = "321321321"
                      },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-24"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail222@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "20",
                         IsAvailable = true,
                         LicenseNumber = 87299,
                         Name = "Paweł",
                         PartTime = false,
                         Position = "Position2",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444555111",
                         Status = "Status2",
                         Street = "Lubelska",
                         StreetNumber = "12",
                         Supervisor = "Supervisor1",
                         Surname = "Tuczeń",
                         Title = "M.D.",
                         Town = "Radom",
                         WorkPhone = "451121322"
                     },
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2016-05-01"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail99@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "84",
                         IsAvailable = true,
                         LicenseNumber = 51111,
                         Name = "Mateusz",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "99 - 876",
                         PrivatePhone = "999222333",
                         Status = "Status2",
                         Street = "Odlewnicza",
                         StreetNumber = "54",
                         Supervisor = "Supervisor1",
                         Surname = "Grzesiak",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "765567897"
                     },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-09-09"),
                         Country = "Poland",
                         Department = "Microbiology",
                         Email = "mai13135@gmail.com",
                         HomeConsult = false,
                         HouseNumber = "341",
                         IsAvailable = true,
                         LicenseNumber = 99700,
                         Name = "Maciej",
                         PartTime = false,
                         Position = "Position23",
                         PostalCode = "23 - 7111",
                         PrivatePhone = "888115999",
                         Status = "Status2",
                         Street = "Chorzowska",
                         StreetNumber = "21",
                         Supervisor = "Supervisor1",
                         Surname = "Arciszewski",
                         Title = "M.D.",
                         Town = "Poznań",
                         WorkPhone = "119991322"
                     }, new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-23"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail1@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "15",
                         IsAvailable = true,
                         LicenseNumber = 77771,
                         Name = "Marek",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "20 - 213",
                         PrivatePhone = "123123123",
                         Status = "Status1",
                         Street = "Hutnicza",
                         StreetNumber = "45",
                         Supervisor = "Supervisor1",
                         Surname = "Wałach",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "321321321"
                     },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-24"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail222@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "20",
                         IsAvailable = true,
                         LicenseNumber = 17777,
                         Name = "Paweł",
                         PartTime = false,
                         Position = "Position2",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444555111",
                         Status = "Status2",
                         Street = "Lubelska",
                         StreetNumber = "12",
                         Supervisor = "Supervisor1",
                         Surname = "Szponar",
                         Title = "M.D.",
                         Town = "Radom",
                         WorkPhone = "451121322"
                     },
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-26"),
                         Country = "Germany",
                         Department = "Cardiology",
                         Email = "example2@gmail.com",
                         HomeConsult = false,
                         HouseNumber = "6",
                         IsAvailable = false,
                         LicenseNumber = 92945,
                         Name = "Paul",
                         PartTime = false,
                         Position = "Position3",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444999211",
                         Status = "Status3",
                         Street = "Biernata",
                         StreetNumber = "87",
                         Supervisor = "Supervisor2",
                         Surname = "Lahm",
                         Title = "M.D.",
                         Town = "Kielce",
                         WorkPhone = "991121322"
                     },
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2016-05-01"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail99@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "84",
                         IsAvailable = true,
                         LicenseNumber = 52110,
                         Name = "Antoni",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "99 - 876",
                         PrivatePhone = "999222333",
                         Status = "Status2",
                         Street = "Odlewnicza",
                         StreetNumber = "54",
                         Supervisor = "Supervisor1",
                         Surname = "Szydło",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "765567897"
                     },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-09-09"),
                         Country = "Poland",
                         Department = "Microbiology",
                         Email = "mai13135@gmail.com",
                         HomeConsult = false,
                         HouseNumber = "341",
                         IsAvailable = true,
                         LicenseNumber = 99705,
                         Name = "Marian",
                         PartTime = false,
                         Position = "Position23",
                         PostalCode = "23 - 7111",
                         PrivatePhone = "888115999",
                         Status = "Status2",
                         Street = "Chorzowska",
                         StreetNumber = "21",
                         Supervisor = "Supervisor1",
                         Surname = "Maniszewski",
                         Title = "M.D.",
                         Town = "Poznań",
                         WorkPhone = "119991322"
                     }, new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-23"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail1@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "15",
                         IsAvailable = true,
                         LicenseNumber = 99971,
                         Name = "Andrzej",
                         PartTime = false,
                         Position = "Position1",
                         PostalCode = "20 - 213",
                         PrivatePhone = "123123123",
                         Status = "Status1",
                         Street = "Hutnicza",
                         StreetNumber = "45",
                         Supervisor = "Supervisor1",
                         Surname = "Kurowski",
                         Title = "M.D.",
                         Town = "Lublin",
                         WorkPhone = "321321321"
                     },

                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-24"),
                         Country = "Poland",
                         Department = "Anaesthetics",
                         Email = "mail222@gmail.com",
                         HomeConsult = true,
                         HouseNumber = "20",
                         IsAvailable = true,
                         LicenseNumber = 13727,
                         Name = "Ksawery",
                         PartTime = false,
                         Position = "Position2",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444555111",
                         Status = "Status2",
                         Street = "Lubelska",
                         StreetNumber = "12",
                         Supervisor = "Supervisor1",
                         Surname = "Mączyński",
                         Title = "M.D.",
                         Town = "Radom",
                         WorkPhone = "451121322"
                     },
                     new Doctor
                     {
                         AvailableDate = DateTime.Parse("2017-08-26"),
                         Country = "Germany",
                         Department = "Cardiology",
                         Email = "example2@gmail.com",
                         HomeConsult = false,
                         HouseNumber = "6",
                         IsAvailable = false,
                         LicenseNumber = 91115,
                         Name = "Philip",
                         PartTime = false,
                         Position = "Position3",
                         PostalCode = "20 - 213",
                         PrivatePhone = "444999211",
                         Status = "Status3",
                         Street = "Biernata",
                         StreetNumber = "87",
                         Supervisor = "Supervisor2",
                         Surname = "Klose",
                         Title = "M.D.",
                         Town = "Kielce",
                         WorkPhone = "991121322"
                     }


                );
                context.SaveChanges();
                }

                // Look for any Hospitals.
                if (!(context.Hospitals.Any()))
                {
                    context.Hospitals.AddRange(
                        new Hospital
                        {
                            ChiefOfMedicine = "Jan Wrona",
                            Country = "Poland",
                            FaxNumber1 = "10987654321",
                            FaxNumber2 = "12345678901",
                            GpsAddress = "51.2599696,22.5588048,17",
                            HelicopterAccess = true,
                            Name = "Uniwersytecki Szpital Dzieciecy",
                            PhoneNumber1 = "789789789",
                            PhoneNumber2 = "987987987",
                            PostalCode = "20-214",
                            SalesRep = "Maciej Kot",
                            Street = "Chodzki",
                            StreetNumber = "6",
                            TeachingHospital = true,
                            Town = "Lublin"
                        },
                        
                        new Hospital
                        {
                            ChiefOfMedicine = "Hans Muller",
                            Country = "Niemcy",
                            FaxNumber1 = "11122233344",
                            FaxNumber2 = "44331144556",
                            GpsAddress = "52.5518604,13.3243754",
                            HelicopterAccess = true,
                            Name = "Deutsches Herzzentrum Berlin",
                            PhoneNumber1 = "999888777",
                            PhoneNumber2 = "666444333",
                            PostalCode = "13353",
                            SalesRep = "Steven Kaufman",
                            Street = "Augustenburger Pl.",
                            StreetNumber = "1",
                            TeachingHospital = true,
                            Town = "Berlin"
                        },
                        new Hospital
                        {
                            ChiefOfMedicine = "Jean Charcot",
                            Country = "Fance",
                            FaxNumber1 = "58311123354",
                            FaxNumber2 = "09809776589",
                            GpsAddress = "48.8370792,2.3628596",
                            HelicopterAccess = true,
                            Name = "Pitie-Salpetriere Hospital",
                            PhoneNumber1 = "123412314",
                            PhoneNumber2 = "190281513",
                            PostalCode = "12341",
                            SalesRep = "Brigide Aboville",
                            Street = "Alle le Vau",
                            StreetNumber = "2",
                            TeachingHospital = true,
                            Town = "Paris"
                        },
                        new Hospital
                        {
                            ChiefOfMedicine = "Zbigniew Koterski",
                            Country = "Poland",
                            FaxNumber1 = "58341142354",
                            FaxNumber2 = "09801212589",
                            GpsAddress = "52.1964618,20.9379486",
                            HelicopterAccess = false,
                            Name = "Szpital Mazovia",
                            PhoneNumber1 = "123123134",
                            PhoneNumber2 = "191231313",
                            PostalCode = "00123",
                            SalesRep = "Joanna Czapska",
                            Street = "Aleja KEN",
                            StreetNumber = "17",
                            TeachingHospital = false,
                            Town = "Warsaw"
                        },
                        new Hospital
                        {
                            ChiefOfMedicine = "Radosław Waryszak",
                            Country = "Poland",
                            FaxNumber1 = "51234124112",
                            FaxNumber2 = "09812412411",
                            GpsAddress = "51.2213954,22.6925532",
                            HelicopterAccess = true,
                            Name = "SPZOZ w Świdniku",
                            PhoneNumber1 = "121233134",
                            PhoneNumber2 = "234243313",
                            PostalCode = "24125",
                            SalesRep = "Adrianna Nowak",
                            Street = "Lotników Polskich",
                            StreetNumber = "18",
                            TeachingHospital = false,
                            Town = "Świdnik"
                        },
                        new Hospital
                        {
                            ChiefOfMedicine = "Andrzej Nowak",
                            Country = "Poland",
                            FaxNumber1 = "51124124112",
                            FaxNumber2 = "09122412411",
                            GpsAddress = "51.4150738,22.2682654",
                            HelicopterAccess = true,
                            Name = "Szpital Kliniczny w Warszawie",
                            PhoneNumber1 = "121233134",
                            PhoneNumber2 = "234243313",
                            PostalCode = "00125",
                            SalesRep = "Amelia Kloc",
                            Street = "Stokłosy",
                            StreetNumber = "2",
                            TeachingHospital = false,
                            Town = "Warsaw"
                        },
                        new Hospital
                        {
                            ChiefOfMedicine = "Janusz Nowak",
                            Country = "Poland",
                            FaxNumber1 = "51112124112",
                            FaxNumber2 = "09122412411",
                            GpsAddress = "51.4150738,22.2682654",
                            HelicopterAccess = true,
                            Name = "ASzpital Kliniczny w Warszawie",
                            PhoneNumber1 = "121233134",
                            PhoneNumber2 = "234243313",
                            PostalCode = "00125",
                            SalesRep = "Amelia Kloc",
                            Street = "Stokłosy",
                            StreetNumber = "23",
                            TeachingHospital = false,
                            Town = "Warsaw"
                        }



                );
                    context.SaveChanges();
                }
                
                if (!(context.HospitalDoctors.Any()))
                { 

                    var hospitals = context
                            .Hospitals
                            .ToDictionary(h => h.Name, h => h.ID);

                    var doctors = context
                        .Doctors
                        .ToDictionary(d => d.LicenseNumber, d => d.ID);

                    context.HospitalDoctors.AddRange(

                     new HospitalDoctor
                     {
                        DateStarted = DateTime.Parse("2011-01-09"),
                        Department = "Anaesthetics",
                        DoctorID = doctors[11111],
                        HomeConsult = false,
                        HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                        PartTime = true,
                        Position = "Position2",
                        Status = "Active",
                        Supervisor = "Supervisor1"
                   
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11111],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     }, 
                     
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11111],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11111],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11115],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11115],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11115],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11115],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12345],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12345],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12345],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12345],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[56712],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[56712],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[56712],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[56712],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[43713],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[43713],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[43713],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[43713],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12353],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12353],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12353],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[12353],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[29303],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[29303],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[29303],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[29303],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[76321],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[76321],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[76321],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[76321],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[87231],
                         HomeConsult = false,
                         HospitalID = hospitals["Uniwersytecki Szpital Dzieciecy"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[87231],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[87231],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[87231],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[54321],
                         HomeConsult = false,
                         HospitalID = hospitals["Deutsches Herzzentrum Berlin"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[54321],
                         HomeConsult = false,
                         HospitalID = hospitals["Pitie-Salpetriere Hospital"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[54321],
                         HomeConsult = false,
                         HospitalID = hospitals["Szpital Mazovia"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },
                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[11111],
                         HomeConsult = false,
                         HospitalID = hospitals["ASzpital Kliniczny w Warszawie"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"
                     },

                     new HospitalDoctor
                     {
                         DateStarted = DateTime.Parse("2011-01-09"),
                         Department = "Anaesthetics",
                         DoctorID = doctors[54321],
                         HomeConsult = false,
                         HospitalID = hospitals["ASzpital Kliniczny w Warszawie"],
                         PartTime = true,
                         Position = "Position2",
                         Status = "Active",
                         Supervisor = "Supervisor1"

                     }



                /*
                                     new HospitalDoctor
                                     {
                                         DateStarted = DateTime.Parse("2011-01-09"),
                                         Department = "Anaesthetics",
                                         DoctorID = 1,
                                         HomeConsult = false,
                                         HospitalID = 2,
                                         PartTime = true,
                                         Position = "Position2",
                                         Status = "Active",
                                         Supervisor = "Supervisor2"

                                     },
                                     new HospitalDoctor
                                     {
                                         DateStarted = DateTime.Parse("2011-01-09"),
                                         Department = "Anaesthetics",
                                         DoctorID = 1,
                                         HomeConsult = false,
                                         HospitalID = 3,
                                         PartTime = true,
                                         Position = "Position2",
                                         Status = "Active",
                                         Supervisor = "Supervisor3"

                                     },
                                     new HospitalDoctor
                                     {
                                         DateStarted = DateTime.Parse("2011-01-09"),
                                         Department = "Anaesthetics",
                                         DoctorID = 1,
                                         HomeConsult = false,
                                         HospitalID = 4,
                                         PartTime = true,
                                         Position = "Position2",
                                         Status = "Active",
                                         Supervisor = "Supervisor4"

                                     }
                                     */
                );
                    context.SaveChanges();
                }
            

            }

        }
    }
}